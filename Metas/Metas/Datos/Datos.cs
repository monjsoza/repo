﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Datos
{
    public class Datosjson
    {
        public string Metas = @"[
                              {
                                'META': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              },
                              {
                                'META': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              },
                              {
                                'META': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              }
                            ]";
        public string MetasIntermediasReprogramadas = @"[
	
                              {
                                'META': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',
                                'Titulo_desde': 'Desde',                                
                                'Fecha_desde': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                                
                              },
                              {
                                'META': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              },
                              {
                                'META': 'ENPDC INTERVENCIÓN 4: 40,112 FAMILIAS CAPACITADAS EN ALMACENAMIENTO Y PROCESAMIENTO DE ALIMENTOS',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              }
							  {
                                'META': 'ENTREGA DE SUBVENCIÓN DE INSUMOS AGROPECUARIOS 2018',
                                'Titulo': 'Meta intermedia',
                                'Fecha_anterior': 'dd/mm/aa',
                                'Nueva_fecha': 'dd/mm/aa'
                              }
                            ]";
        public string MetasDetenidas = @"[
										
										{
										'Numero':8,
										'Subtitulo':'Metas detenidas'
										},
										{
										'Uno1':'DT.07 ACUERDOS,AGENDA INTERNACIONAL Y OTROS INSTRUMENTOS DE VINCULACION - Claudina Valdez,555-55555'
										},
										{
										'Dos2':'15,600 PERSONAS DE LAS COMUNIDADES TÉCNICAS - Claudina Valdez,555-55555'
										},
										{
										'Tres3':'AD.02 FORTALECIMIENTO DE LAS RAS (RED DE ABASTECIMIENTO SOCIAL) - Claudina Valdez,555-55555'
										},
										{
										'Cuatro4':'AD.05 SEGURO DE VIDA PARA 185 MIL PARTICIPANTES DE LOS PROGRAMAS SOCIALES - Claudina Valdez,555-55555'
										},
										{
										'Cinco5':'ASISTENCIA SOCIAL Y COMUNITARIA A FAMILIAS VULNERABLES - Claudina Valdez,555-55555'
										},
										{
										'Seis6':'AD.04 FORTALECIMIENTO DEL SISTEMA TECNOLÓGICO DE ADESS - Claudina Valdez,555-55555'
										},
										{
										'Siete7':'AD.02 FORTALECIMIENTO DE LAS RAS (RED DE ABASTECIMIENTO SOCIAL) - Claudina Valdez,555-55555'
										},
										{
										'Ocho8':'DT.07 ACUERDOS,AGENDA INTERNACIONAL Y OTROS INSTRUMENTOS DE VINCULACION - Claudina Valdez,555-55555'		
										}
	]";
        public string Oportunidades = @"[	
								{
								'TituloFechaDesde':'Desde',
								'Fecha_desde':'//'
								},
								{
								'TituloFechaHasta':'Hasta',
								'Fecha_hasta':'//'
								},
								{
								'TituloOportunidades':'67 Oportunidades'
								},
								{
								'TXT1':'01/10/2018, Experiencias y Buenas Practicas Realizada la pasantia con Frente Parlamentario DT.07 ACUERDOS,AGENDA INTERNACIONAL Y OTROS INSTRUMENTOS DE VINCULACION'
								},
								{
								'TXT2':'18/09/2018,02.2.9 Comunicaciones:Montaje acto inagura   AD.02 FORTALECIMIENTO DE LAS RAS (RED DE ABASTECIMIENTO SOCIAL) seguro de vida contratado para 125.000 actuales + 60.000 Nuevas  AD.05 SEGURO DE VIDA PARA 185 MIL PARTICIPANTES DE LOS PROGRAMAS SOCIALES'
								},
								{
								'TXT3':'21/09/2018,Crear el sistema de registro de Asistencia Social y Comunitario ASISTENCIA SOCIAL Y COMUNITARIA A FAMILIAS VULNERABLES'
								},	
								{
								'TXT4':'28/09/2018, Implementación Core ADESS Web (Autogestión) AD.04 FORTALECIMIENTO DEL SISTEMA'
								}							
	]";
        public string ReporteEjecutidos = @"[
	
												  {
													'Titulo': 'INSTITUCIONES DE LA PROTECCION SOCIAL UTILIZANDO SIUBEN COMO INSTRUMENTO DE FOCALIZACION'                                                                                             
												  }
												  ,
												  {
													'Unito':'JUSTIFICACIÓN DEL ESTADO ACTUAL DE LA META (Normal) a Agosto 2018 esta meta no ha iniciado. Las metas intermedias se encuentran programadas debido a que tienen el Estudio Socioeconómico de Hogares como pre-requisito2)PORCENTAJE ESTIMADO DE AVANCE DE LA META 50%3) PRINCIPALES LOGROS DEL MES QUE SE CIERRA()socializando los pasos a dar y con las instituciones involucradas4)PRINCIPALES LOGROS PREVISTOS PARA EL PROXINO MES () continuar con el desarrollo del Proyecto'
												  },
												  {
													'Subtitulo':'FORTALECIMIENTO DE LAS RAS (RE DE LA ABASTECIMIENTO SOCIAL)',
													'primero':'1 JUSTIFICACIÓN DEL ESTADO ACTUAL DE LA META Normal',
													'segundo':'2 PORCENTAJE AVANCE DE LA META 55%',
													'tercero':'3 PRINCIPALES LOGROS DEL TRIMESTRE QUE SE CIERRA(Abril-Junio)'
													
												  },
												  {
													'Subnumero1':'-Reglamento de Funcionamiento de las Ras aprobado',
													'Subnumero2':'-Reglamento de Funcionamiento de las Ras en digital, listo',
													'Subnumero3':'-Capacitados 374 comerciantes de la RAS (562 comerciantes)',
													'Subnumero4':'-Visitados 164 comercios para nuevas adhesiones'
												  }
                            ]";
        public string MisAlertasYRestricciones = @"[
	
												  {
													    'Titulo1': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO,MUNICI DE GUASTATOYA',
														'alerta':'Alerta en: Iniciado proceso de Licitación:',
														'asunto':'Asunto: Constancia de disponibilidad presupuestaria Para iniciar el proceso de licitación , se debe contar con una constancia de disponibilidad presupuestaria'
												  },
												  {
														'Titulo2': 'ENPDC INTERVENCIÓN 4:40.112 FAMILIAS CAPACITADAS EN ALMACENAMIENTO Y PROCESAMIENTO DE ALIMENTOS',
														'alerta':'Alerta en: Iniciado evento de cotización para el procesamiento de alimentos (frascos):',
														'asunto':'Se necesitan los recursos económicos disponibles a mas tardar al 15 de febrero. Q. 900.000',
														'alerta1':'Alerta en: Llamado a Licitación realizado para adquisición de láminas de zinc para elaboración de Silos',
														'asunto2': 'Asunto: Se necesitan los recursos económicos disponibles a mas tardar al 15 de febrero.  Q. 22.000.000 (reserva estimada para llamado a licitación) '
												  },
												  {
														'Titulo3': 'ENTREGA DE SUBVENCIÓN  DE INSUMOS AGROPECUARIOS 2018',
														'alerta':'Alerta en: Iniciada la actualización de agricultores',
														'asunto':'Asunto: Existe un retraso en la contratación de Extensionistas Rurales. Los extensionistas son necesario para realizar la actualización del registro de los agricultores Por indicaciones del Minisitro la contratación es necesaria antes de que finalice el 1er cuatrimestre. Sin los extensionistas no se realizará el registro de agricultores con lo cual no se podrá entregar el beneficio conllevandoun retraso de mas de 2 meses de la entrega de subvención.'
														
												  },	
												  {
												  'Subtitulo': '12 Alertas y Restricciones'
												  },
												  {
														'Titulo4': 'MEJORAMIENTO SISTEMA DE RIEGO UNIDAD DE RIEGO PALO AMONTONADO, MUNICIPIO DE GUASTATOYA, DEPARTAMENTO',														
														'alerta':'Alerta en: Iniciado proceso de Licitación: ',
														'asunto':'Asunto: Constancia de disponibilidad presupuestaria Para iniciar el proceso de licitación, se debe contar con una constancia de disponibilidad presupuestaria.'
												  },
												  {
														'Subtitulo1': 'QUÉ - INDICACIONES',														
														'Numero1':'1) Gestionar los fondos en Despacho Ministerial, según prioridades.'														
												  },
												  {
														'Quien': 'QUIÉN',														
														'Campo2':'Rodrigo Maldonado',
														'Cuando':'CUANDO',
														'Campo2': '//'			
												  },
												  {
														'botonResuelto':'Resuelta',
														'botonNoResuelto':'No Resuelta'
												  }												  
                            ]";
    }
}
