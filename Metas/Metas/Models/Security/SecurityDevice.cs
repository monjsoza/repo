﻿namespace Metas.Models.Security
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Xamarin.Forms;

    public class SecurityDevice : ContentPage
    {
        public class SeguridadDispositivo
        {
            public string userAgent { get; set; }
            public string ip { get; set; }
        }

        public class RootObject
        {
            public SeguridadDispositivo SeguridadDispositivo { get; set; }
        }
    }
}
