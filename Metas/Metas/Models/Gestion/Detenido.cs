﻿namespace Metas.Models
{
    public class Detenido
    {
        public class RootDetenidosObject
        {
            public int Numero { get; set; }
            public string Subtitulo { get; set; }
            public string Uno1 { get; set; }
            public string Dos2 { get; set; }
            public string Tres3 { get; set; }
            public string Cuatro4 { get; set; }
            public string Cinco5 { get; set; }
            public string Seis6 { get; set; }
            public string Siete7 { get; set; }
            public string Ocho8 { get; set; }
        }
    }
}
