﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Metas.Models
{
    public class Usuariologeado
    {
        public string usuario { get; set; }
        public string password { get; set; }
        public string despacho { get; set; }
        public string codigoFuncionario { get; set; }
        public string codigoInstitucionFuncionario { get; set; }
        public string nombre { get; set; }
        public string nombreInstitucionFuncionario { get; set; }
        public string cargo { get; set; }
        public string nombreArea { get; set; }
        public string permisoDirectorio { get; set; }
        public string codigoArea { get; set; }
        public string codigoAmbiente { get; set; }
        public string destinatario { get; set; }
        public string veContadores { get; set; }
        public string email { get; set; }
        public string estado { get; set; }
        public string nombreInstitucion { get; set; }
        public string cambioPwd { get; set; }
        public string tieneCertificado { get; set; }
        public string sigla { get; set; }
        public string pais1 { get; set; }
        public string pais2 { get; set; }
        public string pais3 { get; set; }
        public string paisDefault { get; set; }
        public string nombrePaisDefault { get; set; }
        public string provincia1 { get; set; }
        public string ciudad1 { get; set; }
        public string ciudad2 { get; set; }
        public string nombreCiudadDefault { get; set; }
        public string ciudad3 { get; set; }
        public string nombreProvinciaDefault { get; set; }
        public string provincia3 { get; set; }
        public string provincia2 { get; set; }
        public string provinciaDefault { get; set; }
        public string ciudadDefault { get; set; }
        public List<string> diasLargos { get; set; }
        public List<string> diasCortos { get; set; }
        public string usuarioBD { get; set; }
        public int intentos { get; set; }
        public string passwordDB { get; set; }
    }

    public class Rootobject
    {
        public Usuariologeado usuariologeado { get; set; }
        public int intentos { get; set; }
        public int errorCode { get; set; }
        public int tieneCentroGestion { get; set; }
        public string nombreEventoAgenda { get; set; }
        public string nombreCorrespondencia { get; set; }
        public string authToken { get; set; }
        public string databaseToken { get; set; }
    }


}