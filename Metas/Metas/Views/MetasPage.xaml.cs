﻿using Metas.ViewsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Metas.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MetasPage : ContentPage
	{
		public MetasPage ()
		{
			InitializeComponent ();
		}

        private void Detenidas_Clicked(object sender, EventArgs e)
        {
            Detenidas();
        }
        public async void Detenidas()
        {
            MainViewModels.GetInstance().Detenidas = new DetenidasViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new DetenidasPage());
        }
        private void Gestion_Clicked(object sender, EventArgs e)
        {
            Gestion();
        }
        public async void Gestion()
        {
            MainViewModels.GetInstance().Gestion = new GestionViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new GestionPage());
        }

        private void Atrasada_Clicked(object sender, EventArgs e)
        {
            Atrasadas();
        }
        public async void Atrasadas()
        {
            MainViewModels.GetInstance().Atrasadas = new AtrasadasViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new AtrasadasPage());
        }
        private void Terminada_Clicked(object sender, EventArgs e)
        {
            Terminada();
        }
        public async void Terminada()
        {
            MainViewModels.GetInstance().Terminada = new TerminadaViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new TerminadaPage());
        }
    }
}