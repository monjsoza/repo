﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static Metas.Models.Detenido;

namespace Metas.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DetenidasPage : ContentPage
	{
		public DetenidasPage ()
		{
			InitializeComponent ();
		}

        public List<string> Dataatrasada { get; set; }
        public List<RootDetenidosObject> ListaAtrasadas()
        {
            Datosjson data = new Datosjson();

            List<RootDetenidosObject> lstmetas = new List<RootDetenidosObject>();
            lstmetas = JsonConvert.DeserializeObject<List<RootDetenidosObject>>(data.MetasDetenidas);


            return lstmetas;
        }

        

    }
}