﻿namespace Metas.ViewsModels
{
    public class MainViewModels
    {
        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }

        public MetasViewModels Metas
        {
            get;
            set;
        }
        public DetenidasViewModels Detenidas
        {
            get;
            set;
        }

        public GestionViewModels Gestion
        {
            get;
            set;
        }
        public AtrasadasViewModels Atrasadas
        {
            get;
            set;
        }
        public TerminadaViewModels Terminada
        {
            get;
            set;
        }


        #endregion
        #region Constructors
        public MainViewModels()
        {
            this.Login = new LoginViewModel();
            this.Metas = new MetasViewModels();
            this.Detenidas = new DetenidasViewModels();
            this.Gestion = new GestionViewModels();
            this.Atrasadas = new AtrasadasViewModels();
            this.Terminada = new TerminadaViewModels();
        }
        
        #endregion
        #region Singleton
        private static MainViewModels instance;

        public static MainViewModels GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModels();
            }

            return instance;
        }
        #endregion
    }
}
