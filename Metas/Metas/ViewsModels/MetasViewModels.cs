﻿namespace Metas.ViewsModels
{
    using Metas.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Net.Http;
    using System.Text;
    using Models;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using static System.Net.Mime.MediaTypeNames;
    using Metas.Views;
    using Xamarin.Forms;
    using Application = Xamarin.Forms.Application;

    public class MetasViewModels:BaseViewModels
    {
        public async void Detenidas()
        {
            MainViewModels.GetInstance().Detenidas = new DetenidasViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new DetenidasPage());
        }
        public  ICommand DetenidasCommand
        {
            get
            {
                return new RelayCommand(Detenidas);
            }
        }

    }
}
