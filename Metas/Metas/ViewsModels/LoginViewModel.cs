﻿namespace Metas.ViewsModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Windows.Input;
    using Xamarin.Forms;

    using Views;
    using System;
    using System.Net.Http;
    using Newtonsoft.Json;
    using Metas.Models;

    using System.Text;
    using System.Collections.Generic;
    using System.IO;
    using Datos;

    public class Lad
    {
        public string userName;
        public string password;
        public int intento;
        public string ip;
        public string userAgent;
    }

    public class LoginViewModel : BaseViewModels
    {
        #region Events   

        #endregion

        #region Atributes
        private string username;
        private string password;
        private bool isrunning;
        private bool isEnabled;
        #endregion

        #region Properties
        public string Username
        {
            get { return this.username; }
            set { SetValue(ref this.username, value); }
        }
        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }

        public bool IsRunning
        {
            get { return this.isrunning; }
            set { SetValue(ref this.isrunning, value); }
        }
        public bool IsRemember
        {
            get;
            set;
        }
       public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }

        #endregion
        #region Construtors
        public LoginViewModel()
        {
            this.IsRemember = true;
            this.IsEnabled = true;
        }
        
        #endregion
        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }


        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Username))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an username",
                    "Accept");
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Password",
                    "Accept");
                this.Password = string.Empty;
                return;
            }

            this.IsRunning = true;
            this.IsEnabled = true;

            HttpClient client = new HttpClient();
            var response = await client.GetAsync("https://www.sigob.org/SigobAPI/api/seguridaddispositivo");
            var dispositivo = await response.Content.ReadAsStringAsync();
            if (dispositivo != null)
            {
                var disarr = dispositivo.Split(',');

                string agente = disarr[0].ToString().Replace("\"", "").Replace("[", "").Replace("]", "");
                string ip = disarr[1].ToString().Replace("\"", "").Replace("[", "").Replace("]", "");

                var item = new Lad { userName = this.Username, password = this.Password, intento = 0, ip = ip, userAgent = agente };
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var postusuario = await client.PostAsync("https://www.sigob.org/SigobAPI/api/Usuario", content);
                var contents = await postusuario.Content.ReadAsStringAsync();
                Rootobject usuario = new Rootobject();
                usuario = JsonConvert.DeserializeObject<Rootobject>(contents);

               


                if (postusuario.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    this.Username = string.Empty;
                    this.Password = string.Empty;

                    MainViewModels.GetInstance().Metas = new MetasViewModels();
                    await Application.Current.MainPage.Navigation.PushAsync(new MetasPage());

                }
                else
                {

                    await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter correct username or password",
                    "Accept");
                    this.Username = string.Empty;
                    this.Password = string.Empty;
                    this.IsRunning = false;
                    this.IsEnabled = true;
                    return;
                }
            }
        }
        #endregion
    }
}
